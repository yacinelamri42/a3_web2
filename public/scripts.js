function time() {
  return (new Date(Date.now())).toTimeString();
}

function date() {
  return (new Date(Date.now())).toDateString();
}

function show_date_time() {
  document.getElementById("time_field").innerHTML = time();
  document.getElementById("date_field").innerHTML = date();
}
