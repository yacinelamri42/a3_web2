const express = require("express");
const path = require("path");
const app = express();
const PORT = 5250;
const fs = require("fs");
const { isUndefined } = require("util");
const cookieSession = require("cookie-session");
const header =
      `<!DOCTYPE html>
<html>
  <head>
    <title>Adopt a dog or cat</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen">
  </head>
  <body>
    <header>
      <a href="./home.html" >
        <img src="./logo.jpeg" alt="logo" >
      </a>
      <div class="time_block">
        <h1>PAC (Pet Adoption Center)</h1><br>
        <p id="date_field"></p>
        <p id="time_field"></p>
      </div>
      <script type="text/javascript" src="scripts.js"></script>
      <script type="text/javascript">
        setInterval(show_date_time, 1);
      </script>
    </header>
    <div class="middle">
      <ul class="nav">
        <li><a href="./home.html">Home</a></li>
        <li><a href="./find.html">Find Dog or Cat</a></li>
        <li><a href="./dog_care.html">Dog Care</a></li>
        <li><a href="./cat_care.html">Cat Care</a></li>
        <li><a href="./giveaway">Have a pet to give away</a></li>
        <li><a href="./create_account.html">Create an account</a></li>
        <li><a href="./logout">Log out</a></li>
        <li><a href="./contact.html">Contact Us</a></li>
      </ul>
      <article>
        <div class="body-content">`
const footer = `
        </div>
      </article>
    </div>
    <footer>
      <a href="privacy.html">Privacy/Disclaimer Statement</a>
    </footer>
  </body>
</html>
`;

app.use(express.json(), express.urlencoded({extended: true}));
app.use(express.static("public"));
app.use(cookieSession({secret: "abc", signed: false}));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "public","home.html"));
});

function findUser(username) {
  const login_data = fs.readFileSync("login", "utf-8");
  const lines = login_data.split("\n");
  for(let i=0; i<lines.length; i++) {
    let user_pass = lines[i].split(":");
    if(user_pass[0] === username) {
      return user_pass[1];
    }
  }
  return false;
}

app.post("/create_account", (req, res) => {
  let data = req.body.user+":"+req.body.pass+'\n';
  if (findUser(req.body.user)) {
    req.session.status = "alreadyExist";
    res.end();
    return;
  }
  // }
  fs.appendFile("login", data, err => {
    if (err) {
      console.error(err);
    } else {
      req.session.status = "created";
      res.redirect("/create_account.html");
      res.end();
    }
  });
});

app.get("/giveaway", (req, res) => {
  if(!req.session.current_user || req.session.current_user === "") {
    res.redirect("/login.html");
  }else {
    console.log(req.session.current_user);
    res.redirect("/giveaway.html");
  }
});

app.post("/giveaway", (req, res) => {
  let friendliness = "";
  if(req.body.cat_friend) {
    friendliness+="cat,"
  }
  if(req.body.dog_friend) {
    friendliness+="dog,"
  }
  if(req.body.for_children) {
    friendliness+="child,"
  }
  let numLines = 0;
  try{
    numLines = fs.readFileSync(path.join(__dirname, "pet_info"), "utf-8").split("\n").length;
  } catch (e) {
  }
  let data = (numLines)+":"+req.session.current_user+":"+req.body.pet_type + ":" + req.body.pet_breed + ":" + req.body.age + ":" + req.body.gender + ":" + friendliness + ":" + req.body.comments + ":" + req.body.owner_name + ":" + req.body.owner_email + "\n";
  fs.appendFileSync(path.join(__dirname, "pet_info"), data);
});

function printPet(pet) {
  console.log(pet);
  return `<div id='pet'><p>${pet[2]}<br>${pet[3]}<br>${pet[4]}<br>${pet[5]}<br>${pet[6]}<br>${pet[7]}<br></p></div>`;
}

app.post("/find", (req, res) => {
  let pets = fs.readFileSync("pet_info", "utf-8").split("\n");
  let finalStr = "";
  for(let i=0; i<pets.length; i++) {
    let pet = pets[i].split(":");
    if (pet[2] !== req.body.pet_type) {
      continue;
    }
    if (req.body.pet_breed !== "doesnt_matter" && pet[3] !== req.body.pet_breed) {
      continue;
    }
    if (req.body.prefered_age !== pet[4]) {
      continue;
    }
    if (req.body.prefered_gender !== pet[5]) {
      continue;
    }
    // console.log(pet);
    finalStr+=printPet(pet);
  }
  res.end(header+finalStr+footer);
});

app.post("/login", (req, res) => {
  const password = findUser(req.body.user);
  if (req.body.pass === password) {
    req.session.current_user = req.body.user;
    res.redirect("/giveaway.html");
  } else if(!password) {
    req.session.status="user_not_found";
  } else {
    req.session.status = "wrong_pass";
  }
  res.end();
});

app.get("/logout", (req, res) => {
  if(req.session && req.session.current_user) {
    req.session.current_user = undefined;
    res.send("<script>alert('You logged out'); window.location.replace('./home.html');</script>");
  } else {
    res.send("<script>alert('you cant do this!!'); window.location.replace('./home.html');</script>");
  }
  res.end();
});

app.listen(5250);
